# Eduardo Espinoza Serrano
# Programa que recibe un número entre 1 y 50 y devuelva la suma de los números consecutivos del 1 hasta ese número.
suma = 0
numero = int(input("Ingresa un numero: "))
if (numero<1 or numero>50):
    print("Ingrese un numero entre 1 y 50")

else:
    for i in range(1,numero+1):
        suma=suma+i

print("La suma de 1 hasta el numero",numero,"es",suma)
